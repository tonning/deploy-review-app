@setup
require __DIR__.'/vendor/autoload.php';
(new \Dotenv\Dotenv(__DIR__, '.env'))->load();

$remoteServerUser = env('SERVER_USER');
$remoteServerDomain = env('SERVER_DOMAIN');

$appName = env('REVIEW_APP_NAME');
$baseDir = env('REVIEW_APP_BASE_DIR');
$appDir = "{$baseDir}/{$appName}";
$baseDomain = env('REVIEW_APP_DOMAIN');
$url = "{$appName}.{$baseDomain}";

$repository = env('REVIEW_APP_RESPOSITORY');
$branch = env('REVIEW_APP_BRANCH');

$usesPassport = env('REVIEW_APP_USES_PASSPORT', false);
$user = env('DEPLOY_USER', 'Unknown');
$slackChannel = env('SLACK_CHANNEL', '#deployments');
$slackWebHook = env('SLACK_DEPLOYMENT_WEBHOOK_URL');

/* Functions */
function logMessage($message) {
    return "echo '\033[32m" .$message. "\033[0m';\n";
}
function logWarning($message) {
    return "echo '\033[0;31m" .$message. "\033[0m';\n";
}
@endsetup

@servers(['local' => '127.0.0.1', 'remote' => "{$remoteServerUser}@{$remoteServerDomain}"])

@macro('deploy')
startDeployment
checkExisting
cloneRepository
checkoutReviewBranch
runComposer
runYarn
generateAssets
createEnvironmentFile
setEnvironmentVariables
generateKey
generatePassportKeys
optimizeInstallation
finishDeploy
@endmacro

@macro('remove')
deleteReviewApp
@endmacro

@task('startDeployment', ['on' => 'remote'])
{{ logMessage("🏃  Starting deployment...") }}
@endtask

@task('checkExisting', ['on' => 'remote'])
cd {{ $baseDir }}
if [ -d {{ $appName }} ]; then
    {{ logMessage("🔍  Removing existing review app...") }}
    rm -rf {{ $appName }}
fi
@endtask

@task('cloneRepository', ['on' => 'remote'])
{{ logMessage("🌀  Cloning repository...") }}
cd {{ $baseDir }}
git clone {{ $repository }} {{ $appName }}
@endtask

@task('checkoutReviewBranch', ['on' => 'remote'])
{{ logMessage("📤  Checking out review branch...") }}
cd {{ $appDir }}
git checkout {{ $branch }}
@endtask

@task('runComposer', ['on' => 'remote'])
{{ logMessage("🎼  Running Composer...") }}
cd {{ $appDir }}
composer install --prefer-dist --no-dev -q
@endtask

@task('createEnvironmentFile', ['on' => 'remote'])
if [ -f {{ $baseDir }}/build/.env.review ]; then
    {{ logMessage("🔑  Copying environment file...") }}
    cp {{ $baseDir }}/build/.env.review {{ $appDir }}/.env
else
    {{ logWarning("❗️  [WARNING] NO ENVIRONMENT FILE FOUND!") }}
    {{ logMessage("🔑  Making generic environment file...") }}
    {{ logMessage("🔑  You should probably fill it out after...") }}
    cd {{ $appDir }}
    cat <<"EOF" >> .env
        APP_NAME="Review"
        APP_ENV=review
        APP_DEBUG=true
        APP_LOG_LEVEL=debug
        APP_URL=http://localhost
    EOF
fi
@endtask

@task('setEnvironmentVariables', ['on' => 'remote'])
{{ logMessage("🔑  Setting App URL...") }}
cd {{ $appDir }}
echo -e "APP_URL={{ $url }}" >> .env
@endtask

@task('generateKey', ['on' => 'remote'])
{{ logMessage("🔑  Generating Application key...") }}
cd {{ $appDir }};
php artisan key:generate
@endtask

@task('generatePassportKeys', ['on' => 'remote'])
if [ {{ $usesPassport }} ]; then
    {{ logMessage("🔑  Generating Passport keys...") }}
    cd {{ $appDir }}
    php artisan passport:keys
fi
@endtask

@task('runYarn', ['on' => 'remote'])
{{ logMessage("📦  Running Yarn...") }}
cd {{ $appDir }};
yarn config set ignore-engines true
yarn
@endtask

@task('generateAssets', ['on' => 'remote'])
{{ logMessage("🌅  Generating assets...") }}
cd {{ $appDir }};
php artisan laroute:generate
npm run production
@endtask

@task('optimizeInstallation', ['on' => 'remote'])
{{ logMessage("✨  Optimizing installation...") }}
cd {{ $appDir }};
php artisan config:clear
php artisan cache:clear
php artisan clear-compiled
php artisan optimize
@endtask

@task('finishDeploy', ['on' => 'local'])
{{ logMessage("🚀  Application successfully deployed @ http://{$url}") }}
@endtask

@task('deleteReviewApp')
{{ logMessage("🚯  Removing review app...") }}
cd {{ $baseDir }}
rm -rf {{ $appName }}
@endtask


@finished
@slack($slackWebHook, $slackChannel, "{$branch} has been deployed as a review app @ {$url} by {$user}")
@endfinished
